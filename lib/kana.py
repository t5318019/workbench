#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Reference: http://www.unicode.org/charts/PDF/U3040.pdf
# hiragana is from \u3040 to \u309f

hiragana = [
u'\u3042\u3044\u3046\u3048\u304a',
u'\u304b\u304d\u304f\u3051\u3053',
u'\u3055\u3057\u3059\u305b\u305d',
u'\u305f\u3061\u3064\u3066\u3068',
u'\u306a\u306b\u306c\u306d\u306e',
u'\u306f\u3072\u3075\u3078\u307b',
u'\u307e\u307f\u3080\u3081\u3082',
u'\u3084\u3086\u3088',
u'\u3089\u308a\u308b\u308c\u308d',
u'\u308f\u3092',
u'\u3093',
]

hiragana = [
'あいうえお',
'かきくけこ',
'さしすせそ',
'たちつてと',
'なにぬねの',
'はひふへほ',
'まみむめも',
'やゆよ',
'らりるれろ',
'わを',
'ん',
]

# Reference: http://www.unicode.org/charts/PDF/U30A0.pdf
# katakana is from \u30a0 to \u30ff

katakana = [
u'\u30a2\u30a4\u30a6\u30a8\u30aa',
u'\u30ab\u30ad\u30af\u30b1\u30b3',
u'\u30b5\u30b7\u30b9\u30bb\u30bd',
u'\u30bf\u30c1\u30c4\u30c6\u30c8',
u'\u30ca\u30cb\u30cc\u30cd\u30ce',
u'\u30cf\u30d2\u30d5\u30d8\u30db',
u'\u30de\u30df\u30e0\u30e1\u30e2',
u'\u30e4\u30e6\u30e8',
u'\u30e9\u30ea\u30eb\u30ec\u30ed',
u'\u30ef\u30f2',
u'\u30f3',
]

katakana = [
'アイウエオ',
'カキクケコ',
'サシスセソ',
'タチツテト',
'ナニヌネノ',
'ハヒフヘホ',
'マミムメモ',
'ヤユヨ',
'ラリルレロ',
'ワヲ',
'ン',
]

if __name__ == '__main__':
    for i in hiragana:
        print i

    for i in katakana:
        print i
