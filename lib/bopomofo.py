#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Reference: http://www.unicode.org/charts/PDF/U3100.pdf
# unicode of bopomofo is from \u3105 to \u3129

initial = '\
ㄅㄆㄇㄈ\
ㄉㄊㄋㄌ\
ㄍㄎㄏ\
ㄐㄑㄒ\
ㄓㄔㄕㄖ\
ㄗㄘㄙ'

medial = 'ㄧㄨㄩ'

final = '\
ㄚㄛㄜㄝ\
ㄞㄟㄠㄡ\
ㄢㄣㄤㄥ\
ㄦ'

bopomofo_keyboard = {
'1': 'ㄅ', 'q': 'ㄆ', 'a': 'ㄇ', 'z': 'ㄈ',
'2': 'ㄉ', 'w': 'ㄊ', 's': 'ㄋ', 'x': 'ㄌ',
'3': 'ˇ', 'e': 'ㄍ', 'd': 'ㄎ', 'c': 'ㄏ',
'4': 'ˋ', 'r': 'ㄐ', 'f': 'ㄑ', 'v': 'ㄒ',
'5': 'ㄓ', 't': 'ㄔ', 'g': 'ㄕ', 'b': 'ㄖ',
'6': 'ˊ', 'y': 'ㄗ', 'h': 'ㄘ', 'n': 'ㄙ',
'7': '˙', 'u': 'ㄧ', 'j': 'ㄨ', 'm': 'ㄩ',
'8': 'ㄚ', 'i': 'ㄛ', 'k': 'ㄜ', ',': 'ㄝ',
'9': 'ㄞ', 'o': 'ㄟ', 'l': 'ㄠ', '.': 'ㄡ',
'0': 'ㄢ', 'p': 'ㄣ', ';': 'ㄤ', '/': 'ㄥ',
'-': 'ㄦ',
}

if __name__ == '__main__':
    print len(initial.decode('utf-8'))
    for i in initial.decode('utf-8'):
        print i, hex(ord(i))

    print len(medial.decode('utf-8'))
    for i in medial.decode('utf-8'):
        print i, hex(ord(i))

    print len(final.decode('utf-8'))
    for i in final.decode('utf-8'):
        print i, hex(ord(i))

    for k, v in bopomofo_keyboard.iteritems():
        print k, v, hex(ord(v.decode('utf-8')))
