#!/usr/bin/env python
import subprocess
import sys

if __name__ == '__main__':
    # Install JRE
    subprocess.check_call(['wget',
                           '--no-cookies',
                           '--no-check-certificate',
                           '--header', 'Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie',
                           'http://download.oracle.com/otn-pub/java/jdk/7u67-b01/jre-7u67-linux-x64.tar.gz'])
    subprocess.check_call(['mkdir', '-p', '/usr/java'])
    subprocess.check_call(['tar', 'xzvf', 'jre-7u67-linux-x64.tar.gz', '-C', '/usr/java', '--no-same-owner'])
    subprocess.check_call(['update-alternatives', '--install', '/usr/bin/java', 'java', '/usr/java/jre1.7.0_67/bin/java', '1'])
    subprocess.check_call(['java', '-version'])

    # Install Eclipse IDE
    subprocess.check_call(['wget', 'http://eclipse.stu.edu.tw/technology/epp/downloads/release/kepler/SR2/eclipse-standard-kepler-SR2-linux-gtk-x86_64.tar.gz'])
    subprocess.check_call(['tar', 'xzvf', 'eclipse-standard-kepler-SR2-linux-gtk-x86_64.tar.gz', '-C', '/opt', '--no-same-owner'])
    subprocess.check_call(['chgrp', '-R', 'sudo', '/opt/eclipse/'])
    subprocess.check_call(['/opt/eclipse/eclipse',
                           '-application', 'org.eclipse.equinox.p2.director',
                           '-repository', 'http://download.eclipse.org/releases/kepler',
                           '-destination', '/opt/eclipse',
                           '-installIU', 'org.eclipse.wst.web_ui.feature.feature.group'])
    subprocess.check_call(['/opt/eclipse/eclipse',
                           '-application', 'org.eclipse.equinox.p2.director',
                           '-repository', 'http://pydev.org/updates',
                           '-destination', '/opt/eclipse',
                           '-installIU', 'org.python.pydev.feature.feature.group'])
    subprocess.check_call(['chmod', '-R', '775', '/opt/eclipse'])

    # Install Debian Packages
    subprocess.check_call(['apt-get', 'update'])
    subprocess.check_call(['apt-get', '-y', 'install', 'vim'])
    subprocess.check_call(['apt-get', '-y', 'install', 'bpython'])
    subprocess.check_call(['apt-get', '-y', 'install', 'git'])
    subprocess.check_call(['apt-get', '-y', 'install', 'python-pip'])

    subprocess.check_call(['apt-get', '-y', 'install', 'build-essential'])
    subprocess.check_call(['apt-get', '-y', 'install', 'python-dev'])

    # Desktop Menu
    f = open('/usr/share/applications/eclipse.desktop', 'w')
    f.write('''[Desktop Entry]
Type=Application
Name=Eclipse
Exec=/opt/eclipse/eclipse
Icon=/opt/eclipse/icon.xpm
Terminal=false
''')
    f.close()

    # Support multi-user
    f = open('/opt/eclipse/configuration/config.ini', 'a')
    f.write('''osgi.configuration.area=@user.home/.eclipse/configuration
osgi.sharedConfiguration.area=/opt/eclipse/configuration
osgi.configuration.cascaded=true
''')
    f.close()

    sys.exit()
