#!/usr/bin/env python
import argparse
import os
import sys


def create_template(proj):
    os.chdir(proj)
    os.mkdir('DEBIAN')

    with open('DEBIAN/control', 'w') as f:
        f.write('''
Package: package-name
Version: 1.0-version-string
Architecture: all
Maintainer: fullname-email
Description: short-description
 long-description
''')

    with open('DEBIAN/preinst', 'w') as f:
        f.write('''#!/bin/bash
case $1 in
"install")
;;
"upgrade")
;;
"abort-upgrade")
;;
esac

exit 0
''')

    with open('DEBIAN/postinst', 'w') as f:
        f.write('''#!/bin/bash
case $1 in
"configure")
set -e
. /usr/share/debconf/confmodule
;;
"abort-remove")
;;
"abort-upgrade")
;;
esac

exit 0
''')

    with open('DEBIAN/prerm', 'w') as f:
        f.write('''#!/bin/bash
case $1 in
"remove")
;;
"upgrade")
;;
"failed-upgrade")
;;
esac

exit 0
''')

    with open('DEBIAN/postrm', 'w') as f:
        f.write('''#!/bin/bash
case $1 in
"abort-install")
;;
"remove")
;;
"purge")
;;
"upgrade")
;;
"abort-upgrade")
;;
"failed-upgrade")
;;
esac

exit 0
''')

    with open('DEBIAN/config', 'w') as f:
        f.write('''#!/bin/bash
set -e
. /usr/share/debconf/confmodule

db_go || true
''')

    with open('DEBIAN/templates', 'w') as f:
        f.write('''
''')

    os.chdir('DEBIAN/')
    for f in os.listdir('.'):
        if f.startswith('p'):
            os.chmod(f, 0775)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create project template for Debian Binary package.')
    parser.add_argument('proj', help='project name')
    args = parser.parse_args()
    args = vars(args)

    proj = args.get('proj')

    try:
        os.mkdir(proj)
        create_template(proj)
    except Exception as e:
        print '\x1b[1;31m%s\x1b[0m' % e

    print '\x1b[1;33m%s\x1b[0m' % 'Use command "dpkg-deb -b <project name> ." to creates a debian archive!'
    sys.exit()
