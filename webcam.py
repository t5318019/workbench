#!/usr/bin/env python
import v4l2
import fcntl
import os
import mmap
import Image

if __name__ == '__main__':
    # Querying Capabilities
    # vd = open('/dev/video0', 'rw')
    vd = os.open('/dev/video0', os.O_RDWR)
    cp = v4l2.v4l2_capability()
    fcntl.ioctl(vd, v4l2.VIDIOC_QUERYCAP, cp)
    for i in cp._fields_:
        print '%s: %s' % (i[0], getattr(cp, i[0]))

    print 'Support Video Capture: %s' % (cp.capabilities & v4l2.V4L2_CAP_VIDEO_CAPTURE > 0)

    # Enumerate image formats
    fmtdesc = v4l2.v4l2_fmtdesc()
    fmtdesc.type = v4l2.V4L2_BUF_TYPE_VIDEO_CAPTURE
    fmt_dict = {}
    try:
        while(fcntl.ioctl(vd, v4l2.VIDIOC_ENUM_FMT, fmtdesc) == 0):
            fmtdesc.index += 1
            fmt_dict[fmtdesc.description] = fmtdesc.pixelformat
    except Exception:
        pass
    print fmt_dict

    # Enumerate frame sizes
    for k, v in fmt_dict.iteritems():
        frmsizeenum = v4l2.v4l2_frmsizeenum()
        frmsizeenum.pixel_format = v
        try:
            print k
            while(fcntl.ioctl(vd, v4l2.VIDIOC_ENUM_FRAMESIZES, frmsizeenum) == 0):
                frmsizeenum.index += 1
                if(frmsizeenum.type == v4l2.V4L2_FRMSIZE_TYPE_DISCRETE):
                    print frmsizeenum.discrete.width, frmsizeenum.discrete.height
        except Exception:
            pass

    # Image Format Negotiation
    fmt = v4l2.v4l2_format()
    fmt.type = v4l2.V4L2_BUF_TYPE_VIDEO_CAPTURE
    fcntl.ioctl(vd, v4l2.VIDIOC_G_FMT, fmt)

    fmt.fmt.pix.pixelformat = v4l2.V4L2_PIX_FMT_YUYV
    fmt.fmt.pix.field = v4l2.V4L2_FIELD_NONE
    fmt.fmt.pix.width = 640
    fmt.fmt.pix.height = 480
    # fmt.fmt.pix.bytesperline = 0
    fcntl.ioctl(vd, v4l2.VIDIOC_S_FMT, fmt)
    print 'Image size: %s bytes' % fmt.fmt.pix.sizeimage

    # Reading Images
    print 'Support Read/Write: %s' % (cp.capabilities & v4l2.V4L2_CAP_READWRITE > 0)
    print 'Support Streaming I/O: %s' % (cp.capabilities & v4l2.V4L2_CAP_STREAMING > 0)

    req = v4l2.v4l2_requestbuffers()
    req.count = 1
    req.type = v4l2.V4L2_BUF_TYPE_VIDEO_CAPTURE
    req.memory = v4l2.V4L2_MEMORY_MMAP
    fcntl.ioctl(vd, v4l2.VIDIOC_REQBUFS, req)

    buf = v4l2.v4l2_buffer()
    buf.type = v4l2.V4L2_BUF_TYPE_VIDEO_CAPTURE
    buf.memory = v4l2.V4L2_MEMORY_MMAP
    buf.index = 0
    fcntl.ioctl(vd, v4l2.VIDIOC_QUERYBUF, buf)
    mm = mmap.mmap(vd, buf.length)

    t = v4l2.v4l2_buf_type(v4l2.V4L2_BUF_TYPE_VIDEO_CAPTURE)
    fcntl.ioctl(vd, v4l2.VIDIOC_STREAMON, t)

    fcntl.ioctl(vd, v4l2.VIDIOC_QBUF, buf)

    fcntl.ioctl(vd, v4l2.VIDIOC_DQBUF, buf)

    size = fmt.fmt.pix.width, fmt.fmt.pix.height
    im = Image.new('RGB', size)
    pixel = im.load()

    for i in range(0, size[1]):
        for j in range(0, size[0] / 2):
            y1 = ord(mm.read(1))
            u = ord(mm.read(1))
            y2 = ord(mm.read(1))
            v = ord(mm.read(1))
            '''
            B = 1.164 * (y1 - 16) + 2.018 * (u - 128)
            G = 1.164 * (y1 - 16) - 0.813 * (v - 128) - 0.391 * (u - 128)
            R = 1.164 * (y1 - 16) + 1.596 * (v - 128)
            pixel[j * 2, i] = int(R), int(G), int(B)

            B = 1.164 * (y2 - 16) + 2.018 * (u - 128)
            G = 1.164 * (y2 - 16) - 0.813 * (v - 128) - 0.391 * (u - 128)
            R = 1.164 * (y2 - 16) + 1.596 * (v - 128)
            pixel[j * 2 + 1, i] = int(R), int(G), int(B)
            '''
            C = (y1 - 16)
            D = (u - 128)
            E = (v - 128)

            R = (1192 * C + 1636 * E) >> 10
            G = (1192 * C - 400 * D - 832 * E) >> 10
            B = (1192 * C + 2064 * D) >> 10
            pixel[j * 2, i] = R, G, B

            C = (y2 - 16)

            R = (1192 * C + 1636 * E) >> 10
            G = (1192 * C - 400 * D - 832 * E) >> 10
            B = (1192 * C + 2064 * D) >> 10
            pixel[j * 2 + 1, i] = R, G, B

    im.save('test.jpg', quality=90)

    fcntl.ioctl(vd, v4l2.VIDIOC_STREAMOFF, t)
    # vd.close()
    os.close(vd)
