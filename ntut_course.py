#! /usr/bin/env python
# -*- coding: utf-8 -*-
# 列出國立臺北科技大學所有的課程
import urllib2
import lxml.html
import re


def get_course(code):
    f = urllib2.urlopen('http://aps.ntut.edu.tw/course/tw/Curr.jsp?format=-1&code=%s' % code)
    resp = f.read()
    html_str = resp.decode('big5')
    html_str = html_str.replace('\n', '')
    # html is an Element instance
    html = lxml.html.fromstring(html_str)

    for tr in html.xpath('//tr'):
        if len(tr) == 6:
            if tr.find('th') is not None:
                continue

            course = {'department': tr[0].text.strip(),
                      'code': tr[1][0].text.strip(),
                      'name-zh_TW': tr[2].text.strip(),
                      'name-en': tr[3].text.strip(),
                      'credit': tr[4].text.strip(),
                      'hour': tr[5].text.strip()
                      }
            print '%(code)s,%(name-zh_TW)s,%(name-en)s' % course

if __name__ == '__main__':
    course = {}

    f = urllib2.urlopen('http://aps.ntut.edu.tw/course/tw/Curr.jsp?format=-0&code=1')
    resp = f.read()
    html_str = resp.decode('big5')
    html_str = html_str.replace('\n', '')
    # html is an Element instance
    html = lxml.html.fromstring(html_str)

    for tr in html.xpath('//tr'):
        if len(tr) == 2 and tr.find('th') is None:
            name = tr[0][1].text
            href = tr[0][1].get('href')
            match = re.search(r'code=(?P<code>\w+)', href)
            code = match.group('code')

            course[code] = name

    for k in course.keys():
        get_course(k)
