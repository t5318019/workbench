#!/usr/bin/env python
import subprocess
import os
import sys

# Cancel network connection in Windows: net use * /delete

if __name__ == '__main__':
    subprocess.check_call('apt-get install -y samba smbclient', shell=True)
    subprocess.check_call('apt-get install -y libpam-smbpass', shell=True)
    if os.path.exists('/etc/samba/smb.conf.bak'):
        sys.exit()
    subprocess.check_call('cp /etc/samba/smb.conf /etc/samba/smb.conf.bak', shell=True)
    # The default is security = user, do not need to configure.
    # subprocess.check_call('sed -ri "s/#\s+security/   security/g" /etc/samba/smb.conf', shell=True)

    f = open('/etc/samba/smb.conf', 'a')
    f.write('''
   [private]
   comment = Private Share
   path = /srv/samba/private
   browsable = yes
   writable = yes
   write list = @sambashare

   [public]
   comment = Public Share
   path = /srv/samba/public
   browsable = yes
   guest ok = yes
   read only = no
   create mask = 0755
   force user = nobody
   force group = nogroup

''')
    f.close()

    # Private share
    subprocess.check_call('mkdir -p /srv/samba/private', shell=True)
    subprocess.check_call('chgrp sambashare /srv/samba/private/', shell=True)
    subprocess.check_call('chmod 2775 /srv/samba/private/', shell=True)
    # Public share
    subprocess.check_call('mkdir -p /srv/samba/public', shell=True)
    subprocess.check_call('chown nobody.nogroup /srv/samba/public/', shell=True)

    subprocess.check_call('restart smbd', shell=True)
    subprocess.check_call('restart nmbd', shell=True)

    subprocess.check_call('useradd -g sambashare -s /usr/sbin/nologin user01 -c user01,,,', shell=True)
    subprocess.check_call('printf "0000\n0000\n" | pdbedit -t -a -u user01', shell=True)
    subprocess.check_call('echo "user01:pass01" | chpasswd', shell=True)

    sys.exit()
