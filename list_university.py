#! /usr/bin/env python
# -*- coding: utf-8 -*-
# 大學校院清單
# list of university
import urllib2
import urlparse


def export_html(category, schools):
    print u'<div>'
    print u'<h3>%s(共%s所)</h3>' % (category, len(schools))
    print u'<ol>'
    for school in schools:
        print u'<li>%s：<a href="http://%s" target="_blank">%s<a></li>' % \
            (school['name'], school['netloc'], school['netloc'])
    print u'</ol>'
    print u'</div>'

if __name__ == '__main__':
    req = urllib2.Request('https://stats.moe.gov.tw/files/school/103/u1_new.txt')

    f = urllib2.urlopen(req)
    data = f.read().decode('utf-16')
    f.close()
    rows = data.split('\r\n')[3:]

    university = []
    institute = []
    normal = []

    for row in rows:
        cols = row.split('\t')
        if len(cols) == 7:
            name = cols[1]
            url = cols[5]
            o = urlparse.urlparse(url)
            school = {'name': name, 'netloc': o.netloc}

            if cols[6] == u'[1]一般':
                if name[-2:] == u'大學':
                    university.append(school)
                elif name[-2:] == u'學院':
                    institute.append(school)
            elif cols[6] == u'[3]師範':
                normal.append(school)

    export_html(u'大學', university)
    export_html(u'學院', institute)
    export_html(u'師範&amp;教育大學', normal)
